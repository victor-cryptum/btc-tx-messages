# https://bitcoin.stackexchange.com/questions/25224/what-is-a-step-by-step-way-to-insert-data-in-op-return/36439#36439
import settings
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging
from decimal import Decimal
from binascii import hexlify
logging.basicConfig()
logging.getLogger("BitcoinRPC").setLevel(logging.DEBUG)

rpc = AuthServiceProxy("http://%s:%s@%s:%s/" % (settings.BLOCKFORCE_BTC_USER,
                                                settings.BLOCKFORCE_BTC_PASSWORD, settings.BLOCKFORCE_BTC_HOST, settings.BLOCKFORCE_BTC_PORT))

sender_address = settings.BTC_SENDER_ADDRESS
sender_priv = settings.BTC_SENDER_PRIV

first_unspent = rpc.listunspent(1, 999999, [sender_address])[0]
txid = first_unspent['txid']
vout = first_unspent['vout']
input_amount = first_unspent['amount']
receive_amount = Decimal("0.0001")
sender_amount = input_amount - receive_amount - Decimal('0.00001')
if sender_amount < 0:
    raise Exception('sender_amount < 0')

# My change address
receive_address = "mnPLHLXaj9rs6hr1WmPJJSvFwtvVyp4BGo"

data = b'Bitcoin is a cryptocurrency invented in 2008 by Satoshi Nakamoto'
if len(data) > 75:
    raise Exception("Can't contain this much data-use OP_PUSHDATA1")
tx = rpc.createrawtransaction([{"txid": txid, "vout": vout}], \
                              {receive_address: receive_amount, \
                               sender_address: sender_amount, "data": hexlify(data).decode()})

tx = rpc.signrawtransactionwithkey(tx, [sender_priv])['hex']
rpc.sendrawtransaction(tx)
